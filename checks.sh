#!/bin/bash

# -- if secret not exist --
# ищем наш секрет
if kubectl get secrets --no-headers -o custom-columns=:metadata.name | grep myweb-db-cred; then
        echo "SECRET FOUND"
else
# если секрет не найден, он будет создан но выводиться эта строка не будет (пароли в отрытом виде)
        kubectl create secret generic myweb-db-cred \
          --from-literal=MYSQL_USER=$MYWEB_DB_USER \
          --from-literal=MYSQL_ROOT_PASSWORD=$MYWEB_DB_PASS \
          --from-literal=MYSQL_DATABASE=$MYWEB_DB_NAME 2> /dev/null
fi

# -- END if secret not exist --

# -- cred for DB --
# Я заметил что мой сайт может "упасть" с ошибкой, из-за того что в первый раз
# не может создать базу с нужным именем. Здесь будем проверять создалась ли БД
# и если нет - создадим ее автоматом. (Да знаю, это ошибку должен разруливать сам сайт. Но так интереснее)

# Создаем файл с параметрами авторизации в БД чтобы не передавать пароли в открытом виде
echo "[client]" > ./config.cnf
echo "user=$MYWEB_DB_USER" >> ./config.cnf
echo "password=$MYWEB_DB_PASS" >> ./config.cnf

# -- END cred for DB --

# -- create or update deploy --
# передаем в helm данные для установки нашего свежего артефакта
helm upgrade -i myweb --set web.imagetag=$MYWEB_IMG_TAG .

# находим pod с нашей БД
MY_DB_POD=$(kubectl get pods --selector=tier=myweb-db-tier --no-headers -o custom-columns=:metadata.name)
echo "POD NAME:" $MY_DB_POD
# смотрим запущен ли уже pod. Если нет, еще немного подождем.
MY_DB_STAT=$(kubectl get pods --selector=tier=myweb-db-tier --no-headers -o custom-columns=:status.phase)

if [[ $MY_DB_STAT != "" && $MY_DB_STAT != "Running" ]]; then
        try=12; cnt=0
	until [[ $try -lt $cnt || $MY_DB_STAT != "Running" ]]; do
		echo "Waiting for the launch DB..." $try
		sleep 5
		$try--
	done
else
		if [[ $MY_DB_STAT == "Running" ]]; then
			echo "DB $MYWEB_DB_NAME IS RUNNING!"
			# копируем файл для авторизации в pod
			kubectl cp ./config.cnf $MY_DB_POD:/config.cnf 
		if kubectl exec $MY_DB_POD -- /bin/bash -c "exec mysqlshow --defaults-extra-file=/config.cnf | grep $MYWEB_DB_NAME"; then
		    echo "DB is already exist! Only update then..."
		else
        	echo "DB NOT FOUND! Create DB..."
        	kubectl exec $MY_DB_POD -- /bin/bash -c "exec mysql --defaults-extra-file=/config.cnf -e 'CREATE DATABASE $MYWEB_DB_NAME;'"
		fi
	else
		# ругаемся если что-то идет не так
		echo "DB POD STATUS:" $MY_DB_STAT
		echo "DB ERROR BLYAT!!!"
	fi
fi

# удаляем файл с данными авторизации.
rm ./config.cnf
